import java.util.ArrayList;

public class Joueur {
	private String nom;
	private Pioche pioche;
    private ArrayList<Carte> main;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public Joueur(String n, Pioche p){
		this.pioche = p;
        this.nom = n;
        main = new ArrayList<Carte>();
        for (int i = 0; i < 6; i++) {
            piocher();
        }
    }
	
	public String getNom(){
		return nom;
	}
	
	public Carte getCarte(int index){
        return main.get(index);
    }
	
	public ArrayList<Carte> getMain(){ 
		return main; 
	}
	
	public void piocher(){
        if(main.size()>6){
            return;
        }
        main.add(pioche.tirerUneCarte());
    }
	
	public int jouerCoup(){
		return 1;
	}
    
    public int jouerCoupPlateau(int n){
    	return n;
    }
	
	public boolean retirerCarte(Carte c){
        return main.remove(c);
    }
	
}
