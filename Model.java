import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.*;

public class Model {

	private int nbOrdi;
	private int nbJoueur;
	public Joueur[] joueurs;
	private Carte carte;
	private CartesJeu plateau;
    public Pioche pioche;
    public int tour;
    public String file = "";
    
    public Model(){
        plateau = new CartesJeu();
        pioche = new Pioche();
        tour = 0;
    }

	public int getNbOrdi() {
		return nbOrdi;
	}

	public void setNbOrdi(int nbOrdi) {
		this.nbOrdi = nbOrdi;
	}

	public int getNbJoueur() {
		return nbJoueur;
	}

	public void setNbJoueur(int nbJoueur) {
		this.nbJoueur = nbJoueur;
	}

	public Carte getCarte() {
		return carte;
	}

	public int getTour() {
		return tour;
	}
	
	public void debut(){
		creerPioche();
		plateau.viderPlateau();
		plateau.ajouterCarte(pioche.tirerUneCarte());
    }
	
	public void creerPioche(){
        pioche.vider();
        List<Carte> listCarte = new ArrayList<Carte>();

        try {
            listCarte = chargerCarte(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Collections.shuffle(listCarte);
        for (Carte carte : listCarte) {
            pioche.ajouterUneCarte(carte);
        }
    }
	
	 public void initJoueurs(){
	        joueurs = new Joueur[nbJoueur+nbOrdi];
	        for (int i = 0; i < nbJoueur; i++){
	            joueurs[i] = new Joueur("joueur "+(i+1), pioche);
	        }
	 }
	
	public List<Carte> chargerCarte(String fileName) throws IOException {
        BufferedReader br;
        char[] tab = new char[2];
        int nbLu;
        Carte carte;
        List<Carte> listCarte = new ArrayList<Carte>();

        br = new BufferedReader(new FileReader(fileName));
        nbLu = br.read(tab,0,2);
        while (nbLu > 0) {
            if (nbLu != 2) throw new IOException("file corrupted");

            String line = br.readLine();
            String[] tab2 = line.split(",");
            carte = new Carte(tab2[0], Integer.parseInt(tab2[1]));

            listCarte.add(carte);
            nbLu = br.read(tab,0,2);
        }
        return listCarte;
    }    
}
