import java.util.ArrayDeque;

public class Pioche {
	ArrayDeque<Carte> pioche;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	 public Pioche() {
	    pioche = new ArrayDeque<Carte>();
	 }
	 
	 public void ajouterUneCarte(Carte carte) {
	       pioche.addLast(carte);
		 }

	 public Carte tirerUneCarte() {
	    return pioche.pop();
	 }

	 public void vider(){
	   pioche.clear();
	 }

}
