import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class Main extends JFrame{

	public static void main(String[] args) {
		JFrame fenetre = new JFrame();
	    fenetre.setTitle("Timeline");
	    fenetre.setSize(1200, 1000);
	    fenetre.setLocationRelativeTo(null);
	    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	    
	    JMenuBar mb = new JMenuBar();
	    fenetre.setJMenuBar(mb);
	    JMenu menu = new JMenu();
	    menu.setText("Paramètres");
	    mb.add(menu);
	    fenetre.setVisible(true);
	    JButton btn = new JButton("Jouer");
	    fenetre.getContentPane().add(btn);
	    fenetre.setVisible(true);
	}

}
