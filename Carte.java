import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.*;
import java.awt.*;

public class Carte {
	private String nom;
    private int date;
    private boolean retourne;
    private Image image;
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Carte(String nom, int date){
        this.nom=nom;
        image = new ImageIcon("Images/Image/" + nom + ".jpg").getImage();
        this.date=date;
        retourne=false;

    }
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public boolean isRetourne() {
		return retourne;
	}

	public void setRetourne(boolean retourne) {
		this.retourne = retourne;
	}
	
	
	
}
