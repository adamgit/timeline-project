import java.util.ArrayList;

public class CartesJeu {

	private ArrayList<Carte> jeucartes;

    public CartesJeu() {
        jeucartes = new ArrayList<Carte>();
    }
    
    public Carte getCarte(int index) {
        return jeucartes.get(index);
    }
    
    public void ajouterCarte(Carte c){
    	jeucartes.add(c);
    }
    
    public void viderPlateau() {
    	jeucartes.clear();
    }
    
    public boolean compareDate(Carte carte, int index) {
        if (index != 0) {
            Carte cartePrev = jeucartes.get(index-1);
            if (carte.getDate()<cartePrev.getDate()) {
                return  false;
            }
        }
        if (index != jeucartes.size()) {
            Carte carteNext = jeucartes.get(index);
            if (carte.getDate()>carteNext.getDate()) {
                return  false;
            }
        }
        return  true;
    }
}
